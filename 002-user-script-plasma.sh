#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://gitlab.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Optional - install xdg-user-dirs and update my home directory.
# This creates the usual folders, Documents, Pictures, etc.
sudo pacman -S --needed --noconfirm xdg-user-dirs
xdg-user-dirs-update --force

# Copy over some of my favorite fonts, themes and icons
sudo [ -d /usr/share/fonts/OTF ] || sudo mkdir /usr/share/fonts/OTF
sudo [ -d /usr/share/fonts/TTF ] || sudo mkdir /usr/share/fonts/TTF
[ -d ~/Wallpapers ] || mkdir ~/Wallpapers
[ -d ~/.local/share/templates ] || mkdir -p ~/.local/share/templates
sudo tar xzf packages/fonts-otf.tar.gz -C /usr/share/fonts/OTF/ --overwrite
sudo tar xzf packages/fonts-ttf.tar.gz -C /usr/share/fonts/TTF/ --overwrite
sudo tar xzf packages/buuf-icons-for-plasma.tar.gz -C /usr/share/icons/ --overwrite
sudo tar xzf packages/buuf3.42.tar.gz -C /usr/share/icons/ --overwrite
tar xzf packages/templates.tar.gz -C ~/.local/share/ --overwrite
sudo tar xzf packages/arch-logo.tar.gz -C /usr/share/icons/ --overwrite
sudo tar xzf packages/smb.conf.tar.gz -C /etc/samba/ --overwrite
tar xzf packages/adapta-home.tar.gz -C ~/ --overwrite
sudo tar xzf packages/share.tar.gz -C /usr/share/ --overwrite
tar xzf packages/zsh.tar.gz -C ~/ --overwrite


cp archarmwallpaper.jpg ~/Wallpapers/

echo " "
echo "All done! Press enter to reboot!"
read -n 1 -s -r -p "Press Enter to reboot or Ctrl+C to stay here."

sudo reboot
