#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://gitlab.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

## Card Partitioning ##

# 1. fdisk /dev/sdX

# 2. Type o. This will clear out any partitions on the drive.

# 3. Type p to list partitions. There should be no partitions left.

# 4. Type n, then p for primary, 1 for the first partition on the drive.

# 5. Press ENTER to accept the default first sector then type +200M for the last sector.

# 6. Type t, then c to set the first partition to type W95 FAT32 (LBA).

# 7. Type n, then p for primary, 2 for the second partition on the drive, and then press ENTER twice to accept the default first and last sector.

# 8. Write the partition table and exit by typing w.

## Card Partitioning ##


# Create and mount boot
sudo mkfs.vfat /dev/sdh1
mkdir boot
sudo mount /dev/sdh1 boot

# Create and mount the ext4 filesystem:
sudo mkfs.ext4 /dev/sdh2
mkdir root
sudo mount /dev/sdh2 root

# Grab the latest build
wget http://os.archlinuxarm.org/os/ArchLinuxARM-rpi-aarch64-latest.tar.gz

# Unpack tarball into local folder
sudo bsdtar -xpf ArchLinuxARM-rpi-aarch64-latest.tar.gz -C root

# Move files from local to SD card
sync

# Move boot files to the first partition:
sudo mv root/boot/* boot

# Update /etc/fstab for the different SD block device compared to the Raspberry Pi 3
sudo sed -i 's/mmcblk0/mmcblk1/g' root/etc/fstab

# Unmount the partitions
sudo umount boot root

## After first boot ##
# Default user alarm, password is alarm.
# The default root password is root.
# pacman-key --init
# pacman-key --populate archlinuxarm

