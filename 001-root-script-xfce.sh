#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://gitlab.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Todo
# Figure out how to kill audit messages

# Change root password
passwd

# Create new user account
echo
echo "What is your username?"
read input1
useradd -m -G wheel,storage,power -s /bin/bash $input1
echo "Set your password."
passwd $input1

# Copy over install to user's directory
cp -r /root/arch-scripted-pi/ /home/$input1/
chown $input1:$input1 /home/$input1/arch-scripted-pi

# Remove alarm user
userdel -f alarm

# Fix the black border issue
echo "disable_overscan=1" >> /boot/config.txt

# Install sudo
pacman -S --needed --noconfirm sudo

# Copy over sudo config
mv /etc/sudoers /etc/sudoers.bak
cp packages/sudoers /etc/sudoers

# Xorg Core
pacman -S --noconfirm --needed xorg-server xorg-apps xorgproto
pacman -S --noconfirm --needed xorg-xinit xorg-twm xorg-xclock xterm

# Video
pacman -S --noconfirm --needed xf86-video-fbdev
pacman -S --noconfirm --needed mesa libva-mesa-driver mesa-vdpau
pacman -S --noconfirm --needed xf86-input-libinput

# Sound
pacman -S --needed --noconfirm pipewire wireplumber gst-plugin-pipewire pipewire-alsa
pacman -S --needed --noconfirm pipewire-jack pipewire-pulse

# Network
pacman -S --noconfirm --needed networkmanager
pacman -S --noconfirm --needed network-manager-applet
systemctl enable --now NetworkManager.service

# VLC rpi edition (trying to avoid a dependency issue)
pacman -S --noconfirm --needed vlc-rpi

# Install lightdm and xfce
sudo pacman -S --noconfirm --needed xfce4 xfce4-goodies lightdm gvfs 
sudo pacman -S --noconfirm --needed lightdm-gtk-greeter light-locker
sudo pacman -S --noconfirm --needed lightdm-gtk-greeter-settings 

# Enable Display Manager
sudo systemctl enable lightdm.service

# Software from 'normal' repositories
pacman -S --noconfirm --needed noto-fonts smbclient ktorrent p7zip liboggz
pacman -S --noconfirm --needed adobe-source-code-pro-fonts neofetch ttf-liberation qt5ct
pacman -S --noconfirm --needed firefox gparted gvfs-smb avahi powerline-fonts
pacman -S --noconfirm --needed file-roller evince gnome-disk-utility lolcat hunspell-en_us
pacman -S --noconfirm --needed openvpn openssh awesome-terminal-fonts conky libogg
pacman -S --noconfirm --needed plank vorbis-tools opus-tools samba

systemctl enable --now avahi-daemon.service

# Adapta Theme
pacman -S --noconfirm --needed adapta-gtk-theme

echo " "
echo "All done! You can now run the user script to"
echo "install custom themes and fonts. If you want."