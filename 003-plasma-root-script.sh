#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://gitlab.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Todo
# Disable console messages
#sudo nano /etc/rc.local
#Add this before 'exit 0':
#Suppress Kernel Messages
#dmesg --console-off

# Change root password
passwd

# Create new user account
echo
echo "What is your username?"
read input1
useradd -m -G wheel,storage,power -s /bin/bash $input1
echo "Set your password."
passwd $input1

# Remove alarm user
# userrm alarm

# Fix the black border issue
# sudo nano /boot/config.txt (just use sed for this)
# Enable this #disable_overscan=1

# Xorg Core
sudo pacman -S --noconfirm --needed xorg-server xorg-apps
sudo pacman -S --noconfirm --needed xorg-xinit xorg-twm 
sudo pacman -S --noconfirm --needed xorg-xclock xterm

# Video
sudo pacman -S --noconfirm --needed xf86-video-fbdev
sudo pacman -S --noconfirm --needed mesa libva-mesa-driver mesa-vdpau
sudo pacman -S --noconfirm --needed xf86-input-libinput

# Sound
sudo pacman -S --needed --noconfirm pipewire wireplumber gst-plugin-pipewire pipewire-alsa
sudo pacman -S --needed --noconfirm pipewire-jack pipewire-pulse

# Network
sudo pacman -S --noconfirm --needed networkmanager
sudo pacman -S --noconfirm --needed network-manager-applet
sudo systemctl enable NetworkManager.service
sudo systemctl start NetworkManager.service

# Install plasma and sddm
sudo pacman -S --noconfirm --needed plasma-meta sddm

# Enable Display Manager
sudo systemctl enable sddm.service

# Software from 'normal' repositories
sudo pacman -S --noconfirm --needed noto-fonts tilix smbclient vlc-rpi
sudo pacman -S --noconfirm --needed adobe-source-code-pro-fonts neofetch
sudo pacman -S --noconfirm --needed firefox geany geany-plugins sudo
sudo pacman -S --noconfirm --needed file-roller evince gvfs-smb
sudo pacman -S --noconfirm --needed gparted gnome-disk-utility

# Adapta Theme
sudo pacman -S --noconfirm --needed adapta-gtk-theme


echo " "
echo "All done!"